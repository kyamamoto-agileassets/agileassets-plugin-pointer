#import "Pointer.h"

#import "KTouchPointerWindow.h"

@implementation Pointer

- (void)pluginInitialize
{

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishLaunching:) name:UIApplicationDidFinishLaunchingNotification object:nil];

}

- (void)finishLaunching:(NSNotification *)notification
{

    KTouchPointerWindowInstall();

}

@end
