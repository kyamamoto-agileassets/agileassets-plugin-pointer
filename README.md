# AgileAssets Cordova Pointer Plugin

Plugin that shows a touch pointer on Cordova app running o iOSreturns your string prefixed with hello.

## Using

Create a new Cordova Project

    $ cordova create hello com.example.helloapp Hello
    
Install the plugin

    $ cordova plugin add https://bitbucket.org/kyamamoto-agileassets/agileassets-plugin-pointer

Install iOS platform

    cordova platform add ios
    
Run the code

    cordova run 

## Contact

[Keiichi Yamamoto](mailto:kyamamoto@agileassets.com)
